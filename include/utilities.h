/*
 * utilities.h
 *
 *  Created on: Mar 2, 2015
 *      Author: ovca
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

#include "common.h"
#include <chrono>
#include <fstream>
#include <streambuf>

static cxsc::real EPS = 0.1;

cxsc::imatrix read_raw_imatrix(std::string filename);
cxsc::rmatrix read_mtx_rmatrix(std::string filename);
bool compare_rmatrix(cxsc::rmatrix result, cxsc::rmatrix expected, cxsc::real eps);
bool compare_imatrix(cxsc::imatrix result, cxsc::imatrix expected, cxsc::real eps);
bool compare_ivector(cxsc::ivector result, cxsc::ivector expected, int n, cxsc::real eps);
bool compare_rvector(cxsc::rvector &result, cxsc::rvector &expected, int n, cxsc::real eps);

template<typename F, typename ...Args>
static double measure_execution_time(F func, Args&&... args)
{
	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	func(std::forward<Args>(args)...);
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() <<std::endl;
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
	loge << duration << logendl;
	return 0;
}

#define SHOW_CMP(res, exp)	\
		logi << logendl;	\
		logi << "result" << logendl;	\
		logi << res << logendl;	\
		logi << "expected" << logendl;	\
		logi << exp << logendl;	\
		logi << logendl;	\

#define CHECK_RESULT_RMATRIX(res, exp, eps) \
		if (compare_rmatrix(res, exp, eps)) \
		{	\
			logs << "Test \"" << __FUNCTION__ << "\" passed" << logendl;	\
		}	\
		else	\
		{	\
			loge << LOGE_BEGIN << "Test \"" << __FUNCTION__ << "\" failed" << LOG_END << logendl;	\
			SHOW_CMP(res, exp)	\
		}	\

#define CHECK_RESULT_IMATRIX(res, exp, eps) \
        if (compare_imatrix(res, exp, eps)) \
        {   \
            logs << "Test \"" << __FUNCTION__ << "\" passed" << logendl;    \
        }   \
        else    \
        {   \
            loge << LOGE_BEGIN << "Test \"" << __FUNCTION__ << "\" failed" << LOG_END << logendl;   \
            SHOW_CMP(res, exp)  \
        }   \

#define CHECK_RESULT_RVECTOR(res, exp, n, eps) \
        if (compare_rvector(res, exp, n, eps)) \
        {   \
            logs << "Test \"" << __FUNCTION__ << "\" passed" << logendl;    \
        }   \
        else    \
        {   \
            loge << LOGE_BEGIN << "Test \"" << __FUNCTION__ << "\" failed" << LOG_END << logendl;   \
            SHOW_CMP(res, exp)  \
        }   \

#define CHECK_RESULT_IVECTOR(res, exp, n, eps) \
        if (compare_ivector(res, exp, n, eps)) \
        {   \
            logs << "Test \"" << __FUNCTION__ << "\" passed" << logendl;    \
        }   \
        else    \
        {   \
            loge << LOGE_BEGIN << "Test \"" << __FUNCTION__ << "\" failed" << LOG_END << logendl;   \
            SHOW_CMP(res, exp)  \
        }   \

#define CHECK_RESULT_INT(res, exp)	\
		if (res == exp)	\
		{	\
			logs << "Test \"" << __FUNCTION__ << "\" passed" << logendl;	\
		}	\
		else	\
		{	\
			loge << LOGE_BEGIN << "Test \"" << __FUNCTION__ << "\" failed" << LOG_END << logendl;	\
			SHOW_CMP(res, exp)	\
		}	\

#define MEASURE_EXECUTION_TIME(func, args...) \
		logi << "Working..." << logendl; \
		logi << "Done. Execution time: " << measure_execution_time(func(args)) << "ms" << logendl;\

#endif /* UTILITIES_H_ */
