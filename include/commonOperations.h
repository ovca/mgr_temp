#ifndef COMMON_OPERATIONS_H_
#define COMMON_OPERATIONS_H_

#include "common.h"

cxsc::rvector sort_vector(cxsc::rvector vector, int dim);
cxsc::rmatrix get_magnitude(const cxsc::imatrix& matrix);
cxsc::rmatrix get_ac(cxsc::imatrix &matrix);
cxsc::rmatrix get_adelta(cxsc::imatrix &matrix);
cxsc::imatrix to_interval(cxsc::rmatrix m1, cxsc::rmatrix m2, int n);
cxsc::ivector to_interval(cxsc::rvector v1, cxsc::rvector v2, int n);

template <typename T>
T trim(T &matrix, int ij)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    T A(n - 1, n - 1);

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (i < ij && j < ij)
            {
                A[i][j] = matrix[i][j];
            }
            else if (i > ij && j > ij)
            {
                A[i - 1][j - 1] = matrix[i][j];
            }
            else if (i < ij && j > ij)
            {
                A[i][j - 1] = matrix[i][j];
            }
            else if (i > ij && j < ij)
            {
                A[i - 1][j] = matrix[i][j];
            }
        }
    }

    return A;
}

template <typename T>
T omp_trim(T &matrix, int ij)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    T A(n - 1, n - 1);

#pragma omp parallel for schedule(static)
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (i < ij && j < ij)
            {
                A[i][j] = matrix[i][j];
            }
            else if (i > ij && j > ij)
            {
                A[i - 1][j - 1] = matrix[i][j];
            }
            else if (i < ij && j > ij)
            {
                A[i][j - 1] = matrix[i][j];
            }
            else if (i > ij && j < ij)
            {
                A[i - 1][j] = matrix[i][j];
            }
        }
    }

    return A;
}

template <typename T>
T getMatrixWithIndexes(T &matrix, std::vector<int> I)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    T A(matrix);
    std::vector<int> toTrim;
    int trimmed = 0;

    for (int i = 1; i <= n; i++)
    {
        bool add = true;
        for (int j = 0; j < I.size(); j++)
        {
            if (I[j] == i)
            {
                add = false;
                break;
            }
        }

        if (true == add)
        {
            toTrim.push_back(i);
        }
    }

    for (int i = 0; i < toTrim.size(); i++)
    {
        A = trim(A, toTrim[i] - i);
    }

    return A;
}
template <typename T>
T omp_getMatrixWithIndexes(T &matrix, std::vector<int> I)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    T A(matrix);
    std::vector<int> toTrim;
    int trimmed = 0;

    for (int i = 1; i <= n; i++)
    {
        bool add = true;
        for (int j = 0; j < I.size(); j++)
        {
            if (I[j] == i)
            {
                add = false;
                break;
            }
        }

        if (true == add)
        {
            toTrim.push_back(i);
        }
    }

    for (int i = 0; i < toTrim.size(); i++)
    {
        A = omp_trim(A, toTrim[i] - i);
    }

    return A;
}

#endif
