#ifndef JACOBI_NEW_H_
#define JACOBI_NEW_H_

#include "common.h"
#include "commonOperations.h"

cxsc::real get_spectral_radius(cxsc::rmatrix &matrix);
cxsc::rvector get_jacobi_eig(cxsc::rmatrix A);
cxsc::rvector get_jacobi_matrix_eig(cxsc::rmatrix A);

#endif
