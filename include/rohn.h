/*
 * rohn.h
 *
 *  Created on: Sep 22, 2014
 *      Author: bowczarek
 */

#ifndef ROHNS_H_
#define ROHNS_H_

#include "common.h"
#include "commonOperations.h"
#include "jacobi.h"

cxsc::ivector get_rohn_eig(cxsc::imatrix matrix);

#endif /* ROHNS_H_ */
