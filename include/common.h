/*
 * common.h
 *
 *  Created on: Sep 8, 2014
 *      Author: ovca
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <rmatrix.hpp>
#include <intmatrix.hpp>
#include <imatrix.hpp>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <omp.h>
#include "log.h"

using namespace std;

#define MAX_ITERATIONS 10000
#define EPSILON 0.0000000000001

#endif /* COMMON_H_ */
