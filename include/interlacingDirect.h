#ifndef INTERLACING_DIRECT_
#define INTERLACING_DIRECT_

#include "common.h"
#include "commonOperations.h"
#include "indexSelection.h"
#include "jacobi.h"

cxsc::ivector get_interlacing_direct_eig(cxsc::imatrix &matrix, iselectf indexSelectionFunc,
                iselectexf indexSelectionExceptFunc);
cxsc::rvector interlacing_direct(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc);


cxsc::ivector get_omp_interlacing_direct_eig(cxsc::imatrix &matrix, iselectf indexSelectionFunc,
                iselectexf indexSelectionExceptFunc);
cxsc::rvector omp_interlacing_direct(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc);

#endif
