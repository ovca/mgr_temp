/*
 * log.h
 *
 *  Created on: Sep 16, 2014
 *      Author: bowczarek
 */

#ifndef LOG_H_
#define LOG_H_

#include <iostream>

#define LOGE_BEGIN "\033[1;31m"	//error
#define LOGS_BEGIN "\033[1;32m" //success
#define LOGI_BEGIN "\033[1;36m" //info
#define LOGW_BEGIN "\033[1;33m" //warning
#define LOGM_BEGIN "\033[1;37m" //message
#define LOG_ENDL "\033[0m\n"
#define LOG_END "\033[0m"

#define logi cout << LOGI_BEGIN
#define loge cerr << LOGE_BEGIN
#define logs cout << LOGS_BEGIN
#define logw cout << LOGW_BEGIN
#define logm cout << LOGM_BEGIN
#define logendl LOG_ENDL
#define logend LOG_END

#define MESSAGE(x) \
		logm << x << logendl; \

#endif /* LOG_H_ */
