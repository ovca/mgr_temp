#ifndef INTERLACING_DIRECT_MATRIX_
#define INTERLACING_DIRECT_MATRIX_

#include "common.h"
#include "commonOperations.h"
#include "indexSelection.h"
#include "jacobi.h"

cxsc::ivector get_interlacing_direct_matrix_eig(cxsc::imatrix &matrix, iselectf indexSelectionFunc,
                iselectexf indexSelectionExceptFunc);
cxsc::rvector interlacing_direct_matrix(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc);


cxsc::ivector get_omp_interlacing_direct_matrix_eig(cxsc::imatrix &matrix, iselectf indexSelectionFunc,
                iselectexf indexSelectionExceptFunc);
cxsc::rvector omp_interlacing_direct_matrix(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc);

#endif
