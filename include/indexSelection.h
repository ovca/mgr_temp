#ifndef INDEX_SELECTION_H_
#define INDEX_SELECTION_H_

#include "common.h"
#include "commonOperations.h"
#include "jacobi.h"

typedef int (iselectf)(cxsc::rmatrix, int kk);
typedef int (iselectexf)(cxsc::rmatrix, std::vector<int>);

//3.2
int arg_max_sum_removed_sqr(cxsc::rmatrix matrix, int kk);
int arg_max_sum_removed_sqr_except(cxsc::rmatrix matrix, std::vector<int> I);
int omp_arg_max_sum_removed_sqr(cxsc::rmatrix matrix, int kk);
int omp_arg_max_sum_removed_sqr_except(cxsc::rmatrix matrix, std::vector<int> I);

//3.1
int arg_min_submatrix_eig(cxsc::rmatrix matrix, int kk);
int arg_min_submatrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I);
int omp_arg_min_submatrix_eig(cxsc::rmatrix matrix, int kk);
int omp_arg_min_submatrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I);

//3.1
int arg_min_submatrix_matrix_eig(cxsc::rmatrix matrix, int kk);
int arg_min_submatrix_matrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I);
int omp_arg_min_submatrix_matrix_eig(cxsc::rmatrix matrix, int kk);
int omp_arg_min_submatrix_matrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I);

#endif
