#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass aghdpl
\begin_preamble

% \documentclass{aghdpl}               % przy kompilacji programem latex
% \documentclass[pdflatex,en]{aghdpl}  % praca w języku angielskim



% dodatkowe pakiety
\usepackage{enumerate}
\usepackage{listings}
\lstloadlanguages{TeX}

%---------------------------------------------------------------------------

\author{Bartosz Wojciech Owczarek}
\shortauthor{Bartosz Wojciech Owczarek}

\titlePL{Równoległe algorytmy obliczania wartości własnych macierzy przedziałowych}
\titleEN{Parallel algorithms for interval eigenvalue problem}

\shorttitlePL{Równoległe algorytmy obliczania wartości własnych macierzy przedziałowych}
\shorttitleEN{Parallel algorithms for interval eigenvalue problem}

\thesistypePL{Praca magisterska}
\thesistypeEN{Master of Science Thesis}

\supervisorPL{dr Bartłomiej Jacek Kubica}
\supervisorEN{Bartłomiej Jacek Kubica Ph.D}

\date{2015}

\facultyPL{Wydział Elektroniki i Technik Informacyjnych}
\facultyEN{Faculty of Electronics and Information Technology}

\acknowledgements{}
\setcounter{tocdepth}{1}
%---------------------------------------------------------------------------
\end_preamble
\options pdflatex
\use_default_options false
\maintain_unincluded_children false
\begin_local_layout
Float
Type listing
GuiName Listing
Placement tbp
Extension lol
NumberWithin none
Style ruled
ListName "List of Listings"
IsPredefined false
UsesFloatPkg true
RefPrefix lst
End
\end_local_layout
\language polish
\language_package default
\inputencoding iso8859-2
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 0
\use_package amssymb 0
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Indeks
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language polish
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
titlepages   
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "podziekowania.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage clearpage
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "wstep.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial1.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial2.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial3.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial4.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial5.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial6.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "rozdzial7.lyx"

\end_inset


\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "artykuly,ksiazki"
options "plain"

\end_inset


\end_layout

\end_body
\end_document
