#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass aghdpl
\options pdflatex
\use_default_options false
\maintain_unincluded_children false
\language polish
\language_package default
\inputencoding iso8859-2
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 0
\use_package amssymb 0
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Indeks
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language polish
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
C-XSC i ATLAS
\end_layout

\begin_layout Section
Wstęp
\end_layout

\begin_layout Standard
Jednym z celów pracy jest implementacja i zrównoleglenie algorytmu wyznaczania
 wartości własnych macierzy przedziałowych.
 Do przygotowania rozwiązania został wybrany język C++, który nie wspiera
 niestety (w standardzie) obliczeń interwałowych.
 Konieczny stał się więc wybór biblioteki, która udostępniałaby odpowiednie
 struktury danych i metody, które potrzebne byłyby w pracy.
 Do wyboru było wiele rozwiązań, lecz skorzystanie z C-XSC okazało się najodpowi
edniejszą opcją.
 Biblioteka ta zapewnia dużą elastyczność i wydajność (przynajmniej w najnowszych wersjach), dobrze współpracuje zarówno z OpenMP, jak i bibliotekami BLAS do operacji na macierzach (o czym poniżej).
 W celu poprawy wydajności, podjęta została także decyzja o skorzystaniu
 z biblioteki typu BLAS.
 Do tego celu wybrana została biblioteka ATLAS.
\end_layout

\begin_layout Section
C-XSC
\end_layout

\begin_layout Standard
C-XSC -- C++ Class Library for eXtended Scientific Computing jest biblioteką
 rozszerzającą język C++ o możliwość wykonywania obliczeń interwałowych.
 Powodów, dla których do przygotowania pracy wybrana została bibliotek C-XSC
 jest kilka.
 Pierwszym, jest fakt, że C-XSC jest rozwiązaniem dość dojrzałym (prace
 nad biblioteką trwają od prawie 20 lat), a jej twórcy określają narzędzie
 jako 
\begin_inset Quotes pld
\end_inset

kompletne
\begin_inset Quotes prd
\end_inset

.
 Na podstawie tego można było wstępnie oszacować użyteczność narzędzia i
 mieć nadzieję, że będzie ono wolne od problemów, z którymi często borykają
 się młode rozwiązania.
 Kolejnym powodem, nie mniej istotnym jest bogactwo klas, metod i rozwiązań,
 które udostępnia biblioteka.
 Należą do nich między innymi:
\end_layout

\begin_layout Itemize
typy liczb rzeczywistych i zespolonych, wraz z wersjami interwałowymi,
\end_layout

\begin_layout Itemize
macierze i wektory,
\end_layout

\begin_layout Itemize
podstawowe operacje arytmetyczne o wysokiej dokładności na wszystkich typach.
\end_layout

\begin_layout Standard
Oprócz powyższych, C-XSC pozwala na zastosowanie bibliotek typu BLAS do
 części obliczeń (np.
 mnożenie macierzy, odwracanie macierzy, itd.).
 Co więcej, niektóre z operacji udostępnianych przez bibliotekę mogą domyślnie być zrównoleg
lone za pomocą OpenMP.
 Skorzystanie z niego jest tylko kwestią odpowiedniej konfiguracji kompilatora
 wspierającego OpenMP i podania odpowiedniej flagi.
\end_layout

\begin_layout Subsection
Organizacja biblioteki i podstawowe założenia
\end_layout

\begin_layout Standard
Cała zawartość biblioteki znajduje się w przestrzeni nazw (
\begin_inset Formula $namespace$
\end_inset

) 
\begin_inset Formula $cxsc$
\end_inset

.
 Mamy w niej dostęp do wszystkich klas i metod z nimi związanych.
 Ważną informacją dla użytkowników biblioteki jest konwencja indeksowana.
 Autorzy zrezygnowali z przyjętej w C++ metody indeksowania od zera, na
 rzecz bardziej matematycznego numerowania od 1 (co jest, dodajmy decyzją nieco kontrowersyjną i sprzyja powstawaniu błędów).
\end_layout

\begin_layout Subsection
Podstawowe typy danych
\end_layout

\begin_layout Standard
C-XSC udostępnia następujące podstawowe typy 
\begin_inset Quotes pld
\end_inset

proste
\begin_inset Quotes prd
\end_inset

:
\end_layout

\begin_layout Itemize
\begin_inset Formula $real$
\end_inset

 -- typ liczb rzeczywistych,
\end_layout

\begin_layout Itemize
\begin_inset Formula $interval$
\end_inset

 -- typ definiujący interwał,
\end_layout

\begin_layout Itemize
\begin_inset Formula $complex$
\end_inset

 -- typ liczb zespolonych,
\end_layout

\begin_layout Itemize
\begin_inset Formula $cinterval$
\end_inset

 -- interwał zespolony.
\end_layout

\begin_layout Standard
Oprócz tych typów, każdy z nich ma wersję o wyższej precyzji.
 Typy te są poprzedzone przedrostkiem 
\begin_inset Quotes pld
\end_inset

l_
\begin_inset Quotes prd
\end_inset

, na przykład 
\begin_inset Quotes pld
\end_inset

l_interval
\begin_inset Quotes prd
\end_inset

.
 Do każdego typu prostego, mamy także typy wektorowe i macierzowe:
\end_layout

\begin_layout Itemize
\begin_inset Formula $rvector$
\end_inset

 -- wektor liczb rzeczywistych
\end_layout

\begin_layout Itemize
\begin_inset Formula $ivector$
\end_inset

 -- wektor interwałów
\end_layout

\begin_layout Itemize
\begin_inset Formula $cvector$
\end_inset

 -- wektor liczb zespolonych
\end_layout

\begin_layout Itemize
\begin_inset Formula $civector$
\end_inset

 -- wektor interwałów zespolonych
\end_layout

\begin_layout Itemize
\begin_inset Formula $intvector$
\end_inset

 -- wektor liczb całkowitych
\end_layout

\begin_layout Itemize
\begin_inset Formula $rmatrix$
\end_inset

 -- macierz rzeczywista
\end_layout

\begin_layout Itemize
\begin_inset Formula $imatrix$
\end_inset

 -- macierz interwałowa
\end_layout

\begin_layout Itemize
\begin_inset Formula $cmatrix$
\end_inset

 -- macierz zespolona
\end_layout

\begin_layout Itemize
\begin_inset Formula $cimatrix$
\end_inset

 -- macierz interwałowa zespolona
\end_layout

\begin_layout Itemize
\begin_inset Formula $intmatrix$
\end_inset

 -- macierz całkowita
\end_layout

\begin_layout Standard
Podobnie jak typy proste, powyższe typy mogą zostać wykorzystane w wersjach
 o wyższej dokładności.
 Sposób postępowania jest w tym przypadku analogiczny.
 Wszystkie powyższe typy macierzowe posiadają także odpowiadające im typy
 
\begin_inset Quotes pld
\end_inset

części
\begin_inset Quotes prd
\end_inset

.
 Są to podmacierze i wiersze/kolumny.
 Te pierwsze posiadają przyrostek 
\begin_inset Quotes pld
\end_inset

_slice
\begin_inset Quotes prd
\end_inset

, a drugie 
\begin_inset Quotes pld
\end_inset

_subv
\begin_inset Quotes prd
\end_inset

.
 C-XSC udostępnia także macierze rzadkie.
 W tym przypadku ich typ jest opatrzony przedrostkiem 
\begin_inset Quotes pld
\end_inset

s
\begin_inset Quotes prd
\end_inset

, np.
 
\begin_inset Quotes pld
\end_inset

srmatrix
\begin_inset Quotes prd
\end_inset

.
Operacje na macierzach rzadkich nie są bezpieczne, jeśli na różnych elementach macierzy mogą operować różne wątki; dlatego tych typów nie użyto w niniejszej pracy.
\end_layout



\begin_layout Subsection
Funkcje biblioteczne
\end_layout

\begin_layout Standard
C-XSC udostępnia kompletny zestaw metod pozwalający operować na zdefiniowanych typach.
 Należą do nich między innymi:
\end_layout

\begin_layout Itemize
Podstawowe operacje matematyczne -- dodawanie, odejmowanie, dzielenie, mnożenie,
 wartość bezwzględna itp.
 Wszystkie operacje są możliwe na większości typów (tam gdzie ma to sens),
 także macierzy, wektorów itp.,
\end_layout

\begin_layout Itemize
Funkcje trygonometryczne -- zwykłe, hiperboliczne i tzw.
 
\begin_inset Quotes pld
\end_inset

convenience functions
\begin_inset Quotes prd
\end_inset

,
\end_layout

\begin_layout Itemize
Operacje macierzowe i wektorowe -- pobieranie wielkości macierzy, transpozycje,
 tworzenie wektora zerowego, macierzy identyczności itp.,
\end_layout

\begin_layout Itemize
Złożone operacje matematyczne -- logarytmy, pierwiastkowanie, potęgowanie
 itp.,
\end_layout

\begin_layout Itemize
Operacje interwałowe (np.
 liczenie środka interwału),
\end_layout

\begin_layout Itemize
Operacje na liczbach zespolonych (np.
 sprzężenie liczby),
\end_layout

\begin_layout Itemize
Funkcje pomocnicze (np.
 pobranie wielkości macierzy, wektora),
\end_layout

\begin_layout Itemize
Wczytywanie i wypisywanie wektorów i macierzy.
\end_layout

\begin_layout Standard
Korzystając z biblioteki trzeba wziąć pod uwagę fakt, że wiele z jej metod
 nie zapewnia bezpieczeństwa w warunkach współbieżności (funkcje nie są
 thread-safe), co może stanowić problem w przypadku ich wykorzystania do
 implementacji algorytmów równoległych.
 Przykładem może być funkcja pierwiastkująca, która operuje na jednej zmiennej
 globalnej.
 Zastosowanie tej funkcji w regionach równoległych prowadzi więc do błędów
 w działaniu.
\end_layout

\begin_layout Subsection
Wykorzystanie BLAS i wbudowane OpenMP
\end_layout

\begin_layout Standard
C-XSC pozwala na skorzystanie z dostępnej w systemie implementacji BLAS
 (kwestia ta zostanie opisana w następnym punkcie).
 Wykorzystanie takiej biblioteki jest zalecane przez dokumentację jako 
\begin_inset Quotes pld
\end_inset

zalecane w celu uzyskania najlepszej wydajności
\begin_inset Quotes prd
\end_inset

.
 Samo uruchomienie C-XSC z włączonym BLAS wymaga tylko ustawienia w przygotowany
m programie zmiennej globalnej 
\begin_inset Formula $opdotprec$
\end_inset

 na 1, skompilowania z flagą 
\begin_inset Formula $-DCXSC\_USE\_BLAS$
\end_inset

 i zlinkowania z odpowiednią biblioteką.
 W tak przygotowanym programie, operacje macierzowe i wektorowe (np.
 operatory mnożenia, dodawania itp.) -- zarówno dla danych zmiennoprzecinkowych, jak i przedziałowych -- korzystają z odpowiednich funkcji BLAS.
 Rozwiązanie to zostanie wykorzystane zostanie w pracy w celu uzyskania
 najlepszej wydajności i przebadania wpływu takiego rozwiązania na efektywność
 zrównoleglenia.
 Kolejnym elementem, który zostanie wykorzystany, jest wbudowane w C-XSC
 zrównoleglenie korzystające z OpenMP.
 Część metod zaimplementowanych w bibliotece zostało przygotowanych także
 w wersjach równoległych.
 Skorzystanie z nich ogranicza się do skompilowania programu z flagą 
\begin_inset Formula $-DCXSC\_USE\_OPENMP$
\end_inset

 i flagą kompilatora linkującą metody OpenMP.
 Również i to rozwiązanie zostanie przetestowane w ramach pracy.
\end_layout

\begin_layout Section
BLAS
\end_layout

\begin_layout Standard
Wraz z rozwojem metod numerycznych, powstało zapotrzebowanie na gotowe implement
acje mniej i bardziej skomplikowanych algorytmów.
 Jednym z najważniejszych dziedzin, które wymagały takich narzędzi, była
 algebra liniowa, a szczególnie operacje wektorowe i macierzowe.
 Dzięki nim programiści mogliby koncentrować się na bardziej złożonych problemac
h, zamiast implementować każdy drobny element programu od zera.
 Istotnym czynnikiem był (i nadal pozostaje) także wysoka wydajność tych
 metod -- szybkość podstawowych operacji istotnie wpływa na działanie całego
 rozwiązania.
 W celu spełnienia tych wymagań, powstała specyfikacja, a następnie implementacj
e BLAS.
 Biblioteki BLAS (Basic Linear Algebra Subprograms) są to zbiory funkcji
 pozwalających na wykonywanie podstawowych operacji arytmetycznych na wektorach
 i macierzach.
 BLAS posiada (jak dotychczas) 3 poziomy:
\end_layout

\begin_layout Enumerate
BLAS 1 - operacje wektor-wektor i wektor-skalar.
\end_layout

\begin_layout Enumerate
BLAS 2 - operacje wektor-macierz i macierz-skalar.
\end_layout

\begin_layout Enumerate
BLAS 3 - operacje macierz-macierz.
\end_layout

\begin_layout Standard
Wszystkie operacje w bibliotece są zoptymalizowane.
 Zależnie od konkretnej dystrybucji może to być optymalizacja dla danej architektury procesorów (np. MKL dla procesorów Intela, IBM ESSL dla PowerPC, Sun Performance Library) bądź ogólna (np. OpenBLAS, ExactBLAS).
 Dzięki temu, za ich pomocą można uzyskać dużo lepszą wydajność niż w przypadku
 operacji zaimplementowanych np.
 w języku C czy C++.
 Kolejne wersje rozszerzyły BLAS także o funkcje takie, jak odwracanie macierzy,
 wyspecjalizowane metody do operowania na macierzach rzadkich i wiele innych.
 Aktualnie istnieje wiele implementacji BLAS, które dostępne są głównie
 dla języków C/C++ i Fortran.
 W niniejszej pracy wykorzystana została biblioteka ATLAS, która zostanie
 przedstawiona w kolejnym punkcie.
\end_layout

\begin_layout Section
ATLAS
\end_layout

\begin_layout Standard
ATLAS -- Automatically Tuned Linear Algebra Software jest nie tyle implementacją BLASa, co programem i biblioteką, które pozwalając wygenerować bibliotekę BLAS, zoptymalizowaną dla danego komputera -- eksperymentalnie, metodą automatycznego strojenia.
 Utworzona biblioteka implementuje operacje wszystkich trzech poziomów w celu uzyskania
 maksymalnej wydajności.
 Biblioteka na podstawie dostępnych informacji o systemie, automatycznie
 dopasowuje sposób działania, aby jak najlepiej wykorzystać możliwości systemu
 na którym jest uruchamiana.
 Wśród informacji które brane są pod uwagę, biblioteka analizuje architekturę
 systemu, ilość pamięci cache procesora, zestaw jego instrukcji i inne elementy.
 Na podstawie zebranych danych, oprócz optymalizacji, metody pozwalają na
 wykorzystanie operacji typowych dla komputerów wektorowych, SIMD (Single
 Instruction Multiple Data).
 Przeprowadzane są także testy badające wpływ kopiowania tablic i wektorów
 i ich możliwy wpływ na poprawę wydajności.
 Kolejnym ważną cechą ATLAS, jest fakt, iż jest on jedną z bibliotek, które
 mogą zostać wykorzystane w połączeniu z C-XSC (zgodnie z oficjalną dokumentacją
 C-XSC).
 Ponadto, w przeciwieństwie do drugiej zalecanej opcji -- Intel MKL, jest
 ona dostępna w większości dystrybucji Linuxa, który został wybrany jako
 środowisko wykorzystane do przygotowania implementacji i badań na potrzeby
 pracy.
 Na podstawie powyższych przesłanek, ATLAS został wybrany jako biblioteka
 BLAS, która zostanie wykorzystana w przygotowywanym rozwiązaniu.
\end_layout

\begin_layout Standard
ATLAS w systemie Linux może zostać zainstalowany na dwa sposoby -- z repozytoriów danej dystrybucji bądź skompilowany ręcznie.
Druga opcja jest znacznie trudniejsza, gdyż wymaga sporej znajomości systemu (m.in., wyłączanie części rdzeni, itd.), aby umożliwić prawidłowe pomiary i optymalizację wygenerowanej biblioteki BLAS.
\end_layout

\end_body
\end_document

