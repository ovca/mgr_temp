#!/bin/bash

for filename in ./*.svg; do
    echo filename is $filename
    w=`inkscape -z -W $filename`
    h=`inkscape -z -H $filename`
    echo w is $w
    echo h is $h
    w=${w%.*}
    h=${h%.*}
    echo cut w is $w
    echo cut h is $h
    w=$((2*$w))
    h=$((2*$h))
    echo double w is $w
    echo double h is $h
    outfilename=${filename%.*}".png"
    inkscape -z -e $outfilename -h $h -w $w $filename
done
