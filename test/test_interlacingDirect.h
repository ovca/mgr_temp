/*
 * test_interlacingDirect.h
 *
 *  Created on: Sep 17, 2014
 *      Author: bowczarek
 */

#ifndef TEST_INTERLACINGDIRECT_H_
#define TEST_INTERLACINGDIRECT_H_
#include "interlacingDirect.h"
#include "indexSelection.h"
#include "utilities.h"

void test_interlacing_direct_maxsum_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(4, 4);
    cxsc::ivector exp_mat(4);
    
    ss << "[2975,3025] [-2015,-1985] [0,0] [0,0]\n"
                    "[-2015,-1985] [4965,5035] [-3020,-2980] [0,0]\n"
                    "[0,0] [-3020,-2980] [6955,7045] [-4025,-3975]\n"
                    "[0,0] [0,0] [-4025,-3975] [8945,9055]" << endl;
    
    ss2 << "[8945.0000,12720.2273]\n"
                    "[2945.0000,9453.4449]\n"
                    "[1708.9320,6281.7216]\n"
                    "[825.2597,3025.0000]" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::ivector res_mat = get_interlacing_direct_eig(in_mat, arg_max_sum_removed_sqr,
                    arg_max_sum_removed_sqr_except);
    
    CHECK_RESULT_IVECTOR(res_mat, exp_mat, 4, EPS)
}

void test_interlacing_direct_maxsum_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(3, 3);
    cxsc::ivector exp_mat(3);
    
    ss << "[0,2] [-7,3] [-2,2]\n"
                    "[-7,3] [4,8] [-3,5]\n"
                    "[-2,2] [-3,5] [1,5]" << endl;
    
    ss2 << "[4.0000,15.3275]\n"
                    "[-2.5616,6.0000]\n"
                    "[-8.9026,2.0000]" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::ivector res_mat = get_interlacing_direct_eig(in_mat, arg_max_sum_removed_sqr,
                    arg_max_sum_removed_sqr_except);
    
    CHECK_RESULT_IVECTOR(res_mat, exp_mat, 3, EPS)
}

void test_interlacing_direct_mineig_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(4, 4);
    cxsc::ivector exp_mat(4);
    
    ss << "[2975,3025] [-2015,-1985] [0,0] [0,0]\n"
                    "[-2015,-1985] [4965,5035] [-3020,-2980] [0,0]\n"
                    "[0,0] [-3020,-2980] [6955,7045] [-4025,-3975]\n"
                    "[0,0] [0,0] [-4025,-3975] [8945,9055]" << endl;
    
    ss2 << "[8945.0000,12720.2273]\n"
                    "[4945.00000,9055.0000]\n"
                    "[2924.5049,6281.7216]\n"
                    "[825.2597,3025.0000]" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::ivector res_mat = get_interlacing_direct_eig(in_mat, arg_min_submatrix_eig, arg_min_submatrix_eig_except);
    
    CHECK_RESULT_IVECTOR(res_mat, exp_mat, 4, EPS)
}

void test_interlacing_direct_mineig_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(3, 3);
    cxsc::ivector exp_mat(3);
    
    ss << "[0,2] [-7,3] [-2,2]\n"
                    "[-7,3] [4,8] [-3,5]\n"
                    "[-2,2] [-3,5] [1,5]" << endl;
    
    ss2 << "[4.0000,15.3275]\n"
                    "[-2.5616,6.0000]\n"
                    "[-8.9026,2.0000]" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;

    cxsc::ivector res_mat = get_interlacing_direct_eig(in_mat, arg_min_submatrix_eig, arg_min_submatrix_eig_except);

    CHECK_RESULT_IVECTOR(res_mat, exp_mat, 3, EPS)
}

#endif /* TEST_INTERLACINGDIRECT_H_ */
