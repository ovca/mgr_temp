/*
 * test_commonOperations.h
 *
 *  Created on: Sep 17, 2014
 *      Author: bowczarek
 */

#ifndef TEST_COMMONOPERATIONS_H_
#define TEST_COMMONOPERATIONS_H_

#include "common.h"
#include "commonOperations.h"
#include "utilities.h"

void test_magnitude_4x4(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(4, 4);
    cxsc::rmatrix exp_mat(4, 4);
    
    ss << "[2975,3025] [-2015,-1985] [0,0] [0,0]\n"
                    "[-2015,-1985] [4965,5035] [-3020,-2980] [0,0]\n"
                    "[0,0] [-3020,-2980] [6955,7045] [-4025,-3975]\n"
                    "[0,0] [0,0] [-4025,-3975] [8945,9055]" << endl;
    
    ss2 << "3025 2015 0 0\n"
                    "2015 5035 3020 0\n"
                    "0 3020 7045 4025\n"
                    "0 0 4025 9055" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = get_magnitude(in_mat);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_magnitude_3x3_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(3, 3);
    cxsc::rmatrix exp_mat(3, 3);
    
    ss << "[0,1] [0,2] [0,0]\n"
                    "[0,0] [0,2] [0,0]\n"
                    "[0,0] [0,0] [0,3]" << endl;
    
    ss2 << "1 2 0\n"
                    "0 2 0\n"
                    "0 0 3" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = get_magnitude(in_mat);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_magnitude_3x3_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(3, 3);
    cxsc::rmatrix exp_mat(3, 3);
    
    ss << "[-1,1] [1,3] [-5,0]\n"
                    "[-6,-5] [-2,7] [0,3]\n"
                    "[0,10] [-7,8] [9,10]" << endl;
    
    ss2 << "1 3 5\n"
                    "6 7 3\n"
                    "10 8 10" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = get_magnitude(in_mat);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_4x4_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(4, 4);
    cxsc::rmatrix exp_mat(3, 3);
    
    ss << "1 2 3 4\n"
                    "5 6 7 8\n"
                    "9 10 11 12\n"
                    "13 14 15 16" << endl;
    
    ss2 << "6 7 8\n"
                    "10 11 12\n"
                    "14 15 16" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 1);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_4x4_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(4, 4);
    cxsc::rmatrix exp_mat(3, 3);
    
    ss << "1 2 3 4\n"
                    "5 6 7 8\n"
                    "9 10 11 12\n"
                    "13 14 15 16" << endl;
    
    ss2 << "1 3 4\n"
                    "9 11 12\n"
                    "13 15 16" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 2);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_4x4_3(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(4, 4);
    cxsc::rmatrix exp_mat(3, 3);
    
    ss << "1 2 3 4\n"
                    "5 6 7 8\n"
                    "9 10 11 12\n"
                    "13 14 15 16" << endl;
    
    ss2 << "1 2 4\n"
                    "5 6 8\n"
                    "13 14 16" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 3);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_4x4_4(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(4, 4);
    cxsc::rmatrix exp_mat(3, 3);
    
    ss << "1 2 3 4\n"
                    "5 6 7 8\n"
                    "9 10 11 12\n"
                    "13 14 15 16" << endl;
    
    ss2 << "1 2 3\n"
                    "5 6 7\n"
                    "9 10 11" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 4);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_3x3_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(3, 3);
    cxsc::rmatrix exp_mat(2, 2);
    
    ss << "1 2 3\n"
                    "4 5 6\n"
                    "7 8 9" << endl;
    
    ss2 << "5 6\n"
                    "8 9" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 1);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_3x3_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(3, 3);
    cxsc::rmatrix exp_mat(2, 2);
    
    ss << "1 2 3\n"
                    "4 5 6\n"
                    "7 8 9" << endl;
    
    ss2 << "1 3\n"
                    "7 9" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 2);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_3x3_3(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(3, 3);
    cxsc::rmatrix exp_mat(2, 2);
    
    ss << "1 2 3\n"
                    "4 5 6\n"
                    "7 8 9" << endl;
    
    ss2 << "1 2\n"
                    "4 5" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 3);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_2x2_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(2, 2);
    cxsc::rmatrix exp_mat(1, 1);
    
    ss << "1 2\n"
                    "3 4" << endl;
    
    ss2 << "4" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 1);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_trim_2x2_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(2, 2);
    cxsc::rmatrix exp_mat(1, 1);
    
    ss << "1 2\n"
                    "3 4" << endl;
    
    ss2 << "1" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rmatrix res_mat = trim(in_mat, 2);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(4, 4);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "1 2 3 4\n"
                    "6 7 8 9\n"
                    "11 12 13 14\n"
                    "16 17 18 19" << endl;
    
    indexes.push_back(1);
    indexes.push_back(2);
    indexes.push_back(3);
    indexes.push_back(4);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(3, 3);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "7 8 9\n"
                    "12 13 14\n"
                    "17 18 19" << endl;
    
    indexes.push_back(2);
    indexes.push_back(3);
    indexes.push_back(4);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_3(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(2, 2);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "7 9\n"
                    "17 19" << endl;
    
    indexes.push_back(2);
    indexes.push_back(4);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_4(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(1, 1);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "25" << endl;
    
    indexes.push_back(5);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_rev_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(4, 4);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "1 2 3 4\n"
                    "6 7 8 9\n"
                    "11 12 13 14\n"
                    "16 17 18 19" << endl;
    
    indexes.push_back(4);
    indexes.push_back(3);
    indexes.push_back(2);
    indexes.push_back(1);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_rev_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(3, 3);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "7 8 9\n"
                    "12 13 14\n"
                    "17 18 19" << endl;
    
    indexes.push_back(4);
    indexes.push_back(3);
    indexes.push_back(2);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_rev_3(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(2, 2);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "7 9\n"
                    "17 19" << endl;
    
    indexes.push_back(4);
    indexes.push_back(2);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_rand_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(4, 4);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "1 2 3 4\n"
                    "6 7 8 9\n"
                    "11 12 13 14\n"
                    "16 17 18 19" << endl;
    
    indexes.push_back(1);
    indexes.push_back(4);
    indexes.push_back(2);
    indexes.push_back(3);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

void test_submatrix_5x5_rand_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rmatrix exp_mat(3, 3);
    std::vector<int> indexes;
    
    ss << "1 2 3 4 5\n"
                    "6 7 8 9 10\n"
                    "11 12 13 14 15\n"
                    "16 17 18 19 10\n"
                    "21 22 23 24 25" << endl;
    
    ss2 << "7 8 9\n"
                    "12 13 14\n"
                    "17 18 19" << endl;
    
    indexes.push_back(4);
    indexes.push_back(2);
    indexes.push_back(3);
    
    ss >> in_mat;
    ss2 >> exp_mat;
    cxsc::rmatrix res_mat = getMatrixWithIndexes(in_mat, indexes);
    
    CHECK_RESULT_RMATRIX(res_mat, exp_mat, EPS)
}

#endif /* TEST_COMMONOPERATIONS_H_ */
