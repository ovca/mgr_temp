/*
 * tests.h
 *
 *  Created on: Sep 15, 2014
 *      Author: bowczarek
 */

#ifndef PERFORMANCE_TESTS_H_
#define PERFORMANCE_TESTS_H_

#include "common.h"
#include "utilities.h"
#include "indexSelection.h"
#include "interlacingDirect.h"
#include "interlacingDirectMatrix.h"


#include "jacobi.h"

using namespace std;

//void test_performance_jacobi(std::string testfile)
//{
//	MESSAGE("USING " + testfile + " AS TEST DATA")
//	logi << "reading test matrix..." << logendl;
//    cxsc::rmatrix test_mat = read_mtx_rmatrix(testfile);
//    logi << "test matrix read successfull." << logendl;
//
//    MESSAGE("STARTING PARALLEL RUN")
//    //MEASURE_EXECUTION_TIME(omp_get_jacobi_eig(test_mat));
//    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
//    omp_get_jacobi_eig(test_mat);
//	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
//	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
//	logs << duration << logendl;
//
//
//    MESSAGE("STARTING SEQUENTIAL RUN")
//    //MEASURE_EXECUTION_TIME(get_jacobi_eig(test_mat));
//    t1 = std::chrono::high_resolution_clock::now();
//    get_jacobi_eig(test_mat);
//	t2 = std::chrono::high_resolution_clock::now();
//	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
//	logs << duration << logendl;
//}

void test_performance_interlacing_direct_arg_max_sum_removed_sqr(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_eig(test_mat, omp_arg_max_sum_removed_sqr,
                        omp_arg_max_sum_removed_sqr_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_eig(test_mat, arg_max_sum_removed_sqr,
                        arg_max_sum_removed_sqr_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;
}

void test_performance_interlacing_direct_arg_min_submatrix_eig(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_eig(test_mat, omp_arg_min_submatrix_eig,
                        omp_arg_min_submatrix_eig_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_eig(test_mat, arg_min_submatrix_eig,
    					arg_min_submatrix_eig_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;
}

void test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_matrix_eig(test_mat, omp_arg_max_sum_removed_sqr,
                        omp_arg_max_sum_removed_sqr_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_matrix_eig(test_mat, arg_max_sum_removed_sqr,
                        arg_max_sum_removed_sqr_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;
}

void test_performance_interlacing_direct_matrix_arg_min_submatrix_eig(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_matrix_eig(test_mat, omp_arg_min_submatrix_matrix_eig,
                        omp_arg_min_submatrix_matrix_eig_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_matrix_eig(test_mat, arg_min_submatrix_matrix_eig,
    					arg_min_submatrix_matrix_eig_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;
}

void RUN_TESTS(void)
{
    MESSAGE("RUNNING PERFORMANCE TESTS")

	MESSAGE("STARTING WITHOUT BLAS")
	MESSAGE("TESTING ORIGINAL ALGORITHM")
	MESSAGE("interlacing direct(arg_min_submatrix_eig) performance tests")
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/5x5.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/10x10.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/15x15.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/20x20.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/25x25.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/30x30.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/35x35.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/40x40.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/45x45.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/50x50.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/60x60.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/70x70.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/80x80.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/90x90.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/100x100.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/125x125.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/150x150.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/175x175.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/200x200.txt");

	MESSAGE("interlacing direct(arg_max_sum_removed_sqr) performance tests")
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/5x5.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/10x10.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/15x15.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/20x20.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/25x25.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/30x30.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/35x35.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/40x40.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/45x45.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/50x50.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/60x60.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/70x70.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/80x80.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/90x90.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/100x100.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/125x125.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/150x150.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/175x175.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/200x200.txt");

	MESSAGE("TESTING MATRIX ALGORITHM")
	MESSAGE("interlacing direct(arg_min_submatrix_eig) performance tests")
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/5x5.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/10x10.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/15x15.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/20x20.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/25x25.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/30x30.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/35x35.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/40x40.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/45x45.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/50x50.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/60x60.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/70x70.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/80x80.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/90x90.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/100x100.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/125x125.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/150x150.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/175x175.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/200x200.txt");

	MESSAGE("interlacing direct(arg_max_sum_removed_sqr) performance tests")
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/5x5.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/10x10.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/15x15.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/20x20.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/25x25.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/30x30.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/35x35.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/40x40.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/45x45.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/50x50.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/60x60.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/70x70.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/80x80.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/90x90.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/100x100.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/125x125.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/150x150.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/175x175.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/200x200.txt");

	cxsc::opdotprec = 1;

	MESSAGE("STARTING WITH BLAS")
	MESSAGE("TESTING ORIGINAL ALGORITHM")
	MESSAGE("interlacing direct(arg_min_submatrix_eig) performance tests")
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/5x5.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/10x10.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/15x15.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/20x20.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/25x25.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/30x30.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/35x35.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/40x40.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/45x45.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/50x50.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/60x60.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/70x70.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/80x80.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/90x90.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/100x100.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/125x125.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/150x150.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/175x175.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/200x200.txt");

	MESSAGE("interlacing direct(arg_max_sum_removed_sqr) performance tests")
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/5x5.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/10x10.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/15x15.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/20x20.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/25x25.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/30x30.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/35x35.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/40x40.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/45x45.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/50x50.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/60x60.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/70x70.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/80x80.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/90x90.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/100x100.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/125x125.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/150x150.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/175x175.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/200x200.txt");

	MESSAGE("TESTING MATRIX ALGORITHM")
	MESSAGE("interlacing direct(arg_min_submatrix_eig) performance tests")
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/5x5.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/10x10.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/15x15.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/20x20.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/25x25.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/30x30.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/35x35.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/40x40.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/45x45.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/50x50.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/60x60.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/70x70.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/80x80.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/90x90.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/100x100.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/125x125.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/150x150.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/175x175.txt");
	test_performance_interlacing_direct_matrix_arg_min_submatrix_eig("test/data/200x200.txt");

	MESSAGE("interlacing direct(arg_max_sum_removed_sqr) performance tests")
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/5x5.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/10x10.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/15x15.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/20x20.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/25x25.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/30x30.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/35x35.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/40x40.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/45x45.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/50x50.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/60x60.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/70x70.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/80x80.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/90x90.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/100x100.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/125x125.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/150x150.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/175x175.txt");
	test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr("test/data/200x200.txt");

    MESSAGE("PERFORMANCE TESTS COMPLETE")
}

#endif /* PERFORMANCE_TESTS_H_ */
