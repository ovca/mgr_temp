/*
 * tests.h
 *
 *  Created on: Sep 15, 2014
 *      Author: bowczarek
 */

#ifndef TESTS_H_
#define TESTS_H_

#include "common.h"
#include "utilities.h"

#include "test_commonOperations.h"
#include "test_indexSelection.h"
#include "test_jacobi.h"
#include "test_rohn.h"
#include "test_interlacingDirect.h"
#include "test_omp_interlacingDirect.h"

using namespace std;

void RUN_TESTS(void)
{
    MESSAGE("RUNNING TESTS")
    
//    MESSAGE("magnitude tests")
//    test_magnitude_3x3_1();
//    test_magnitude_3x3_2();
//    test_magnitude_4x4();
//
    MESSAGE("jacobi tests")
//    test_jacobi_2x2();
    test_jacobi_3x3();
    test_jacobi_4x4();
    test_jacobi_5x5_1();
    test_jacobi_5x5_2();
//
//    MESSAGE("trim tests")
//    test_trim_2x2_1();
//    test_trim_2x2_2();
//
//    test_trim_3x3_1();
//    test_trim_3x3_2();
//    test_trim_3x3_3();
//
//    test_trim_4x4_1();
//    test_trim_4x4_2();
//    test_trim_4x4_3();
//    test_trim_4x4_4();
//
//    MESSAGE("submatrix tests")
//    test_submatrix_5x5_1();
//    test_submatrix_5x5_2();
//    test_submatrix_5x5_3();
//    test_submatrix_5x5_4();
//    test_submatrix_5x5_rev_1();
//    test_submatrix_5x5_rev_2();
//    test_submatrix_5x5_rev_3();
//    test_submatrix_5x5_rand_1();
//    test_submatrix_5x5_rand_2();
//
//    MESSAGE("index selection tests")
//    test_max_squares_sum_7x7_1();
//    test_max_squares_sum_7x7_2();
//    test_max_squares_sum_7x7_3();
//    test_max_squares_sum_7x7_4();
//    test_max_squares_sum_7x7_5();
//    test_max_squares_sum_7x7_6();
//    test_max_squares_sum_7x7_7();
//    test_max_squares_sum_7x7_8();
//    test_max_squares_sum_6x6_1();
//    test_max_squares_sum_6x6_2();
//    test_max_squares_sum_6x6_3();
//    test_max_squares_sum_6x6_4();
//    test_max_squares_sum_6x6_5();
//    test_max_squares_sum_6x6_6();
//    test_max_squares_sum_except_6x6_1();
//    test_max_squares_sum_except_6x6_2();
//    test_max_squares_sum_except_6x6_3();
//    test_max_squares_sum_except_6x6_4();
//
//    MESSAGE("eigenvalues using Rohn's theorem tests")
//    test_rohn_1();
//    test_rohn_2();
    
    MESSAGE("eigenvalues using interlacing direct tests")
    test_interlacing_direct_maxsum_1();
    test_interlacing_direct_maxsum_2();
    test_interlacing_direct_mineig_1();
    test_interlacing_direct_mineig_2();
//    test_omp_interlacing_direct_maxsum_1();
//    test_omp_interlacing_direct_maxsum_2();

    MESSAGE("TESTS COMPLETE")
}

#endif /* TESTS_H_ */
