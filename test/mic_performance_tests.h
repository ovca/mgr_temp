/*
 * mic_performance_tests.h
 *
 *  Created on: Sep 15, 2014
 *      Author: bowczarek
 */

#ifndef MIC_PERFORMANCE_TESTS_H_
#define MIC_PERFORMANCE_TESTS_H_

#include "common.h"
#include "utilities.h"
#include "indexSelection.h"
#include "interlacingDirect.h"
#include "interlacingDirectMatrix.h"


#include "jacobi.h"

using namespace std;

void test_performance_interlacing_direct_arg_max_sum_removed_sqr(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_eig(test_mat, omp_arg_max_sum_removed_sqr,
                        omp_arg_max_sum_removed_sqr_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_eig(test_mat, arg_max_sum_removed_sqr,
                        arg_max_sum_removed_sqr_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;

	std::flush(std::cout);
	std::cout.flags();
}

void test_performance_interlacing_direct_arg_min_submatrix_eig(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_eig(test_mat, omp_arg_min_submatrix_eig,
                        omp_arg_min_submatrix_eig_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_eig(test_mat, arg_min_submatrix_eig,
    					arg_min_submatrix_eig_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;

	std::flush(std::cout);
	std::cout.flags();
}

void test_performance_interlacing_direct_matrix_arg_max_sum_removed_sqr(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_matrix_eig(test_mat, omp_arg_max_sum_removed_sqr,
                        omp_arg_max_sum_removed_sqr_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_matrix_eig(test_mat, arg_max_sum_removed_sqr,
                        arg_max_sum_removed_sqr_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;

	std::flush(std::cout);
	std::cout.flags();
}

void test_performance_interlacing_direct_matrix_arg_min_submatrix_eig(std::string testfile)
{
	MESSAGE("USING " + testfile + " AS TEST DATA")
	logi << "reading test matrix..." << logendl;
    cxsc::imatrix test_mat = read_raw_imatrix(testfile);
    logi << "test matrix read successfull." << logendl;

    MESSAGE("STARTING PARALLEL RUN")
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    get_omp_interlacing_direct_matrix_eig(test_mat, omp_arg_min_submatrix_matrix_eig,
                        omp_arg_min_submatrix_matrix_eig_except);
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;


    MESSAGE("STARTING SEQUENTIAL RUN")
    t1 = std::chrono::high_resolution_clock::now();
    get_interlacing_direct_matrix_eig(test_mat, arg_min_submatrix_matrix_eig,
    					arg_min_submatrix_matrix_eig_except);
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
	logs << duration << logendl;

	std::flush(std::cout);
	std::cout.flags();
}

void RUN_TESTS(void)
{
	std::ofstream out("mic_performance_tests.log");
	std::cout.rdbuf(out.rdbuf());

	MESSAGE("RUNNING PERFORMANCE TESTS")

	MESSAGE("interlacing direct(arg_min_submatrix_eig) performance tests")
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/5x5.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/15x15.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/25x25.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/35x35.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/45x45.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/60x60.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/80x80.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/100x100.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/150x150.txt");
	test_performance_interlacing_direct_arg_min_submatrix_eig("test/data/200x200.txt");

	MESSAGE("interlacing direct(arg_max_sum_removed_sqr) performance tests")
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/5x5.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/15x15.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/25x25.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/35x35.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/45x45.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/60x60.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/80x80.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/100x100.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/150x150.txt");
	test_performance_interlacing_direct_arg_max_sum_removed_sqr("test/data/200x200.txt");

    MESSAGE("PERFORMANCE TESTS COMPLETE")
}

#endif /* MIC_PERFORMANCE_TESTS_H_ */
