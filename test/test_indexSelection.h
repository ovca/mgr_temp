/*
 * test_indexSelection.h
 *
 *  Created on: Sep 17, 2014
 *      Author: bowczarek
 */

#ifndef TEST_INDEXSELECTION_H_
#define TEST_INDEXSELECTION_H_

#include "common.h"
#include "indexSelection.h"
#include "utilities.h"

void test_max_squares_sum_7x7_1(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 1;
    
    ss << "999 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_2(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 2;
    
    ss << "1 1 1 1 1 1 1\n"
                    "1 999 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_3(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 3;
    
    ss << "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 999 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_4(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 4;
    
    ss << "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 999 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_5(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 5;
    
    ss << "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 999 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_6(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 6;
    
    ss << "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 999 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_7(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 7;
    
    ss << "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 1\n"
                    "1 1 1 1 1 1 999" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_7x7_8(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(7, 7);
    int exp_index = 4;
    
    ss << "1 2 3 4 3 2 1\n"
                    "2 2 3 4 3 2 1\n"
                    "3 3 3 4 3 2 1\n"
                    "4 4 4 4 3 2 1\n"
                    "3 3 3 3 3 2 1\n"
                    "2 2 2 2 2 2 1\n"
                    "1 1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 7);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_6x6_1(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    int exp_index = 1;
    
    ss << "999 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 6);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_6x6_2(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    int exp_index = 2;
    
    ss << "1 1 1 1 1 1\n"
                    "1 999 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 6);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_6x6_3(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    int exp_index = 3;
    
    ss << "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 999 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 6);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_6x6_4(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    int exp_index = 4;
    
    ss << "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 999 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 6);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_6x6_5(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    int exp_index = 5;
    
    ss << "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 999 1\n"
                    "1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 6);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_6x6_6(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    int exp_index = 6;
    
    ss << "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 999" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr(in_mat, 6);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_except_6x6_1(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    vector<int> except;
    int exp_index = 5;
    
    except.push_back(2);
    except.push_back(3);
    except.push_back(4);
    
    ss << "1 1 1 1 1 1\n"
                    "1 999 1 1 1 1\n"
                    "1 1 9999 1 1 1\n"
                    "1 1 1 99999 1 1\n"
                    "1 1 1 1 99 1\n"
                    "1 1 1 1 1 1" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr_except(in_mat, except);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_except_6x6_2(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    vector<int> except;
    int exp_index = 6;
    
    except.push_back(2);
    except.push_back(3);
    except.push_back(4);
    
    ss << "1 1 1 1 1 1\n"
                    "1 999 1 1 1 1\n"
                    "1 1 9999 1 1 1\n"
                    "1 1 1 99999 1 1\n"
                    "1 1 1 1 99 1\n"
                    "1 1 1 1 1 999" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr_except(in_mat, except);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_except_6x6_3(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    vector<int> except;
    int exp_index = 1;
    
    except.push_back(2);
    except.push_back(3);
    except.push_back(4);
    
    ss << "9999 1 1 1 1 1\n"
                    "1 999 1 1 1 1\n"
                    "1 1 9999 1 1 1\n"
                    "1 1 1 99999 1 1\n"
                    "1 1 1 1 99 1\n"
                    "1 1 1 1 1 999" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr_except(in_mat, except);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

void test_max_squares_sum_except_6x6_4(void)
{
    stringstream ss;
    cxsc::rmatrix in_mat(6, 6);
    vector<int> except;
    int exp_index = 4;
    
    except.push_back(1);
    except.push_back(6);
    
    ss << "9999 1 1 1 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 99 1 1 1\n"
                    "1 1 1 999 1 1\n"
                    "1 1 1 1 1 1\n"
                    "1 1 1 1 1 9999" << endl;
    
    ss >> in_mat;
    
    int res_index = arg_max_sum_removed_sqr_except(in_mat, except);
    
    CHECK_RESULT_INT(res_index, exp_index)
}

#endif /* TEST_INDEXSELECTION_H_ */
