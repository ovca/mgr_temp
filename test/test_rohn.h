/*
 * test_rohn.h
 *
 *  Created on: Sep 17, 2014
 *      Author: bowczarek
 */

#ifndef TEST_ROHN_H_
#define TEST_ROHN_H_

#include "rohn.h"
#include "utilities.h"

void test_rohn_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(4, 4);
    cxsc::ivector exp_mat(4);
    
    ss << "[2975,3025] [-2015,-1985] [0,0] [0,0]\n"
                    "[-2015,-1985] [4965,5035] [-3020,-2980] [0,0]\n"
                    "[0,0] [-3020,-2980] [6955,7045] [-4025,-3975]\n"
                    "[0,0] [0,0] [-4025,-3975] [8945,9055]" << endl;
    
    ss2 << "[12560.6296,12720.4331]\n"
                    "[6984.5571,7144.3606]\n"
                    "[3309.9466,3469.7501]\n"
                    "[825.2597,985.0632]" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::ivector res_mat = get_rohn_eig(in_mat);
    
    CHECK_RESULT_IVECTOR(res_mat, exp_mat, 4, EPS)
}

void test_rohn_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::imatrix in_mat(3, 3);
    cxsc::ivector exp_mat(3);
    
    ss << "[0,2] [-7,3] [-2,2]\n"
                    "[-7,3] [4,8] [-3,5]\n"
                    "[-2,2] [-3,5] [1,5]" << endl;
    
    ss2 << "[-2.2298,16.0881]\n"
                    "[-6.3445,11.9734]\n"
                    "[-8.9026,9.4154]" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::ivector res_mat = get_rohn_eig(in_mat);
    
    CHECK_RESULT_IVECTOR(res_mat, exp_mat, 3, EPS)
}

#endif /* TEST_ROHN_H_ */
