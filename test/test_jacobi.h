/*
 * test_jacobi.h
 *
 *  Created on: Sep 17, 2014
 *      Author: bowczarek
 */

#ifndef TEST_JACOBI_H_
#define TEST_JACOBI_H_

#include "common.h"
#include "jacobi.h"
#include "utilities.h"

void test_jacobi_5x5_1(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rvector exp_mat(5);
    
    ss << "1 2 3 0 0\n"
                    "2 3 2 3 0\n"
                    "3 2 3 2 3\n"
                    "0 3 2 3 2\n"
                    "0 0 3 2 3" << endl;
    
    ss2 << "9.886818\n"
                    "3.565670\n"
                    "2.675858\n"
                    "-1.072576\n"
                    "-2.055770" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rvector res_mat = get_jacobi_eig(in_mat);
    
    CHECK_RESULT_RVECTOR(res_mat, exp_mat, 5, EPS)
}

void test_jacobi_5x5_2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(5, 5);
    cxsc::rvector exp_mat(5);
    
    ss << "1 1 2 0 0\n"
                    "1 2 3 4 0\n"
                    "2 3 4 5 5\n"
                    "0 4 5 5 5\n"
                    "0 0 5 5 6" << endl;
    
    ss2 << "16.295\n"
                    "3.958\n"
                    "1.685\n"
                    "-1.644\n"
                    "-2.294" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rvector res_mat = get_jacobi_eig(in_mat);
    
    CHECK_RESULT_RVECTOR(res_mat, exp_mat, 5, EPS)
}

void test_jacobi_2x2(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(2, 2);
    cxsc::rvector exp_mat(2);
    
    ss << "0 2\n"
                    "2 0" << endl;
    
    ss2 << "2\n"
                    "-2" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rvector res_mat = get_jacobi_eig(in_mat);
    
    CHECK_RESULT_RVECTOR(res_mat, exp_mat, 2, EPS)
}

void test_jacobi_3x3(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(3, 3);
    cxsc::rvector exp_mat(3);
    
    ss << "1 2 0\n"
                    "2 2 0\n"
                    "0 0 3" << endl;
    
    ss2 << "3.561\n"
                    "3.0\n"
                    "-0.561" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rvector res_mat = get_jacobi_eig(in_mat);
    
    CHECK_RESULT_RVECTOR(res_mat, exp_mat, 3, EPS)
}

void test_jacobi_4x4(void)
{
    stringstream ss;
    stringstream ss2;
    cxsc::rmatrix in_mat(4, 4);
    cxsc::rvector exp_mat(4);
    
    ss << "4242.788 2828.507 0.0 0.0\n"
                    "2828.507 7071.241 4242.735 0.0\n"
                    "0.0 4242.735 9899.699 5656.965\n"
                    "0.0 0.0 5656.965 12728.16" << endl;
    
    ss2 << "17876.767\n"
                    "9990.889\n"
                    "4794.102\n"
                    "1280.130" << endl;
    
    ss >> in_mat;
    ss2 >> exp_mat;
    
    cxsc::rvector res_mat = get_jacobi_eig(in_mat);
    
    CHECK_RESULT_RVECTOR(res_mat, exp_mat, 4, EPS)
}

#endif /* TEST_JACOBI_H_ */
