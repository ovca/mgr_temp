#!/bin/bash

#g++ main.cpp src/*.cpp -I./include -lcxsc -o main -g -fopenmp -std=c++11
#g++ test/tests.cpp src/*.cpp -I./include -DCXSC_USE_BLAS -lcxsc -latlas -lblas -o tests -g -fopenmp -std=c++11
#g++ test/performance_tests.cpp src/*cpp -I./include -DCXSC_USE_BLAS -lcxsc -latlas -lblas  -o performance_tests -g -fopenmp -std=c++11
g++ test/tests.cpp src/*.cpp -I./include -DCXSC_USE_OPENMP -DCXSC_USE_BLAS -lcxsc -latlas -lblas -o tests -g -fopenmp -std=c++11
g++ test/performance_tests.cpp src/*cpp -I./include -DCXSC_USE_OPENMP -DCXSC_USE_BLAS -lcxsc -latlas -lblas  -o performance_tests -g -fopenmp -std=c++11


echo "Build finished."
