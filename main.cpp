/*
 * main.cpp
 *
 *  Created on: Jun 21, 2014
 *      Author: ovca
 */

#include "common.h"
#include "interlacingDirect.h"
#include "interlacingIndirect.h"
#include "rohn.h"
#include "indexSelection.h"
#include "utilities.h"
#include <stdlib.h>
#include <time.h>

using namespace std;


int main(int argc, char *argv[])
{
    stringstream ss;

    srand (time(NULL));
    ss << argv[1];

    std::string n;

    ss >> n;

    int N = std::stoi(n);

    cxsc::imatrix A(N,1);
    cxsc::imatrix B(1,N);
    cxsc::imatrix out(N,N);


    for (int i = 1; i <= N; i++)
    {
    	int a = std::rand() % 1000;
    	int b = std::rand() % 1000;
    	int c = std::rand() % 1000;
    	int d = std::rand() % 1000;

    	if (rand() % 2 == 0)
    	{
    		a = -a;
    	}
    	if (rand() % 2 == 0)
		{
			b = -b;
		}
    	if (rand() % 2 == 0)
		{
			c = -c;
		}
    	if (rand() % 2 == 0)
		{
			d = -d;
		}

    	cxsc::interval inta;
    	stringstream ssa;
    	if (a > b)
    	{
    		ssa << "[" << b << "," << a << "]";
    	}
    	else
    	{
    		ssa << "[" << a << "," << b << "]";
    	}
    	ssa >> inta;
    	A[i][1] = inta;

    	cxsc::interval intb;
    	stringstream ssb;
    	if (c > d)
		{
    		ssb << "[" << d << "," << c << "]";
		}
		else
		{
    		ssb << "[" << c << "," << d << "]";
		}
    	ssb >> intb;
		B[1][i] = intb;
    }

    out = A * B;

    for (int i = 1; i <= N; i++)
    {
    	for (int j = i; j <= N; j++)
    	{
    		out[j][i] = out[i][j];
    	}
    }

	ofstream myfile;
	myfile.open (argv[2]);
	myfile << N;
	myfile << out;
	myfile.close();

    return 0;
}
