#include "jacobi.h"

cxsc::real get_spectral_radius(cxsc::rmatrix &matrix)
{
    cxsc::rvector eigen = get_jacobi_eig(matrix);
    int dim = cxsc::ColLen(matrix);

    cxsc::real maxVal = cxsc::MinReal;
    for (int i = 1; i <= dim; i++)
    {
    	cxsc::real tmpVal = cxsc::abs(eigen[i]);
        if (maxVal < tmpVal)
        {
            maxVal = tmpVal;
        }
    }

    return maxVal;
}

cxsc::rvector get_diagonal(cxsc::rmatrix &matrix)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    cxsc::rvector d(n);
    
    for (int i = 1; i <= n; i++)
    {
        d[i] = matrix[i][i];
    } 

    return d;
}

bool is_diagonal(cxsc::rmatrix &matrix)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= i; j++)
        {
            if (i != j && EPSILON < cxsc::abs(matrix[i][j]))
            {
                return false;
            }
        }
    }

    return true;
}

void get_max_off_diagonal(cxsc::rmatrix &A, int &r, int &c)
{
    int cols = cxsc::ColLen(A);
    int rows = cxsc::RowLen(A);
    int n = cols;

    assert(cols == rows);

    cxsc::real maxElement = -cxsc::Infinity;

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= i; j++)
        {
        	cxsc::real tmpElement = cxsc::abs(A[i][j]);
            if (i != j && maxElement < tmpElement)
            {
                maxElement = tmpElement;
                r = i;
                c = j;
            }
        }
    }

    assert(maxElement != -cxsc::Infinity);
}

cxsc::rvector get_jacobi_matrix_eig (cxsc::rmatrix A)
{
    int cols = cxsc::ColLen(A);
    int rows = cxsc::RowLen(A);
    int n = cols;

    assert(cols == rows);

    while (false == is_diagonal(A))
    {
        int i;
        int j;
        get_max_off_diagonal(A, i, j);

        cxsc::real c;
        cxsc::real s;

        if (EPSILON >= cxsc::abs(A[j][j] - A[i][i]))
        {
            c = cxsc::Sqrt2_real / 2;
            s = c;
        }
        else
        {
            cxsc::real angle = cxsc::atan(2 * A[i][j] / (A[j][j] - A[i][i])) / 2;
            c = cxsc::cos(angle);
            s = cxsc::sin(angle);
        }

        cxsc::rmatrix G = cxsc::Id(A);

		G[i][i] = c;
		G[i][j] = s;
		G[j][i] = -s;
		G[j][j] = c;

		A = cxsc::transp(G)*A*G;
    }

    return sort_vector(get_diagonal(A), n);
}

cxsc::rvector get_jacobi_eig (cxsc::rmatrix A)
{
    int cols = cxsc::ColLen(A);
    int rows = cxsc::RowLen(A);
    int n = cols;

    assert(cols == rows);

    while (false == is_diagonal(A))
    {
        cxsc::rmatrix S(A);
        int i;
        int j;
        get_max_off_diagonal(S, i, j);

        cxsc::real c;
        cxsc::real s;

        if (EPSILON >= cxsc::abs(S[j][j] - S[i][i]))
        {
            c = cxsc::Sqrt2_real / 2;
            s = c;
        }
        else
        {
            cxsc::real angle = cxsc::atan(2 * S[i][j] / (S[j][j] - S[i][i])) / 2;
            c = cxsc::cos(angle);
            s = cxsc::sin(angle);
        }

        A[i][j] = (c * c - s * s) * S[i][j] + s * c * (S[i][i] - S[j][j]);
        A[j][i] = (c * c - s * s) * S[i][j] + s * c * (S[i][i] - S[j][j]);

        A[j][j] = s * s * S[i][i] + 2 * s * c * S[i][j] + c * c * S[j][j];
        A[i][i] = c * c * S[i][i] - 2 * s * c * S[i][j] + s * s * S[j][j];

        for (int k = 1; k <= n; k++)
        {
            if (k != i && k != j)
            {
                A[i][k] = c * S[i][k] - s * S[j][k];
                A[k][i] = c * S[i][k] - s * S[j][k];
                A[j][k] = s * S[i][k] + c * S[j][k];
                A[k][j] = s * S[i][k] + c * S[j][k];
            }
        }
    }

    return sort_vector(get_diagonal(A), n);
}
