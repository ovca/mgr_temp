#include "commonOperations.h"

//TODO replace with something better
cxsc::rvector sort_vector(cxsc::rvector vector, int dim)
{
    bool switched = true;

    while (true == switched)
    {
        switched = false;
        for (int i = 2; i <= dim; i++)
        {
            if (vector[i] > vector[i - 1])
            {
                switched = true;
                cxsc::real tmp = vector[i - 1];
                vector[i - 1] = vector[i];
                vector[i] = tmp;
            }
        }
    }

    return vector;
}

cxsc::rmatrix get_magnitude(const cxsc::imatrix &matrix)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    cxsc::rmatrix magnitude(cols, rows);

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            cxsc::interval val = matrix[i][j];
            cxsc::real inf = cxsc::Inf(val);
            cxsc::real sup = cxsc::Sup(val);
            cxsc::real mag = cxsc::max(sup, -inf);
            
            magnitude[i][j] = mag;
        }
    }

    return magnitude;
}

cxsc::rmatrix get_ac(cxsc::imatrix &matrix)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::rmatrix supA(dim, dim);
    cxsc::rmatrix infA(dim, dim);
    cxsc::rmatrix Ac(dim, dim);
    
    supA = cxsc::Sup(matrix);
    infA = cxsc::Inf(matrix);
    
    Ac = (supA + infA) / 2.0;
    
    return Ac;
}

cxsc::rmatrix get_adelta(cxsc::imatrix &matrix)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::rmatrix supA(dim, dim);
    cxsc::rmatrix infA(dim, dim);
    cxsc::rmatrix Adelta(dim, dim);
    
    supA = cxsc::Sup(matrix);
    infA = cxsc::Inf(matrix);
    
    Adelta = (supA - infA) / 2.0;
    
    return Adelta;
}

cxsc::ivector to_interval(const cxsc::rvector v1, const cxsc::rvector v2, int n)
{
    cxsc::ivector res(n);
    for (int i = 1; i <= n; i++)
    {
        if (v1[i] < v2[i])
        {
            res[i] = cxsc::interval(v1[i], v2[i]);
        }
        else
        {
            res[i] = cxsc::interval(v2[i], v1[i]);
        }
    }

    return res;
}

cxsc::imatrix to_interval(cxsc::rmatrix m1, cxsc::rmatrix m2, int n)
{
    cxsc::imatrix res(n, n);
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (m1[i][j] < m2[i][j])
            {
                res = cxsc::interval(m1[i][j], m2[i][j]);
            }
            else
            {
                res = cxsc::interval(m2[i][j], m1[i][j]);
            }
        }
    }

    return res;
}
