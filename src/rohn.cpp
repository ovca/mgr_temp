#include "rohn.h"
#include <thread>
#include <chrono>

cxsc::ivector get_rohn_eig(cxsc::imatrix matrix)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::rmatrix Ac = get_ac(matrix);
    cxsc::rmatrix Adelta = get_adelta(matrix);
    cxsc::real rho = get_spectral_radius(Adelta);
    cxsc::rvector eigen = get_jacobi_eig(Ac);
    cxsc::rvector inf(eigen);
    cxsc::rvector sup(eigen);
    
    for (int i = 1; i <= dim; i++)
    {
        inf[i] -= rho;
        sup[i] += rho;
    }

    return to_interval(inf, sup, dim);
}
