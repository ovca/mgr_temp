/*
 * utilities.cpp
 *
 *  Created on: Mar 2, 2015
 *      Author: ovca
 */

#include "utilities.h"

cxsc::imatrix read_raw_imatrix(std::string filename)
{
	std::ifstream infile(filename);
	std::string line;
	std::stringstream ss;
	int N;

	infile >> N;

	cxsc::imatrix matrix(N,N);

	while (std::getline(infile, line))
	{
		ss << line;
		ss << "\n";
	}

	ss >> matrix;

	logw << "RAW read warn: no input checks implemented - matrix may not be square." << logendl;

	return matrix;
}

cxsc::rmatrix read_mtx_rmatrix(std::string filename)
{
	std::ifstream infile(filename);

	int i;
	int j;
	cxsc::real element;

	infile >> i >> j >> element;

	if (i != j)
	{
		cxsc::rmatrix ret;
		loge << "MTX read fail: matrix is not square." << logendl;
		return ret;
	}

	cxsc::rmatrix matrix(i,j);

	for (int p = 1; p < i; p++)
	{
		for (int q = 1; q < j; q++)
		{
			matrix[p][q] = 0;
		}
	}

	while (infile >> i >> j >> element)
	{
		matrix[i][j] = element;
	}

	return matrix;
}

bool compare_rmatrix(cxsc::rmatrix result, cxsc::rmatrix expected, cxsc::real eps)
{
    int cols = cxsc::ColLen(result);
    int rows = cxsc::RowLen(result);

    for (int i = 1; i <= cols; i++)
    {
        for (int j = 1; j <= rows; j++)
        {
            if (false == (result[i][j] >= expected[i][j] + eps) && (result[i][j] <= expected[i][j] - eps))
            {
                return false;
            }
        }
    }

    return true;
}

bool compare_imatrix(cxsc::imatrix result, cxsc::imatrix expected, cxsc::real eps)
{
    int cols = cxsc::ColLen(result);
    int rows = cxsc::RowLen(result);
    cxsc::rmatrix supR = cxsc::Sup(result);
    cxsc::rmatrix infR = cxsc::Inf(result);
    cxsc::rmatrix supE = cxsc::Sup(expected);
    cxsc::rmatrix infE = cxsc::Inf(expected);

    for (int i = 1; i <= cols; i++)
    {
        for (int j = 1; j <= rows; j++)
        {
	   if (infR[i][j] > infE[i][j] + eps || infR[i][j] < infE[i][j] - eps
          	 || supR[i][j] > supE[i][j] + eps || supR[i][j] < supE[i][j] - eps)
            {
                return false;
            }
        }
    }

    return true;
}

bool compare_ivector(cxsc::ivector result, cxsc::ivector expected, int n, cxsc::real eps)
{
    cxsc::rvector supR = cxsc::Sup(result);
    cxsc::rvector infR = cxsc::Inf(result);
    cxsc::rvector supE = cxsc::Sup(expected);
    cxsc::rvector infE = cxsc::Inf(expected);

    for (int i = 1; i <= n; i++)
    {
        if (infR[i] > infE[i] + eps || infR[i] < infE[i] - eps
              || supR[i] > supE[i] + eps || supR[i] < supE[i] - eps)
        {
            return false;
        }
    }

    return true;
}

bool compare_rvector(cxsc::rvector &result, cxsc::rvector &expected, int n, cxsc::real eps)
{
    for (int i = 1; i <= n; i++)
    {
        if (false == (result[i] >= expected[i] + eps) && (result[i] <= expected[i] - eps))
        {
            return false;
        }
    }

    return true;
}
