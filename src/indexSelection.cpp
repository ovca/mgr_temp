#include "indexSelection.h"

int arg_max_sum_removed_sqr(cxsc::rmatrix matrix, int kk)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real maxSum = cxsc::MinReal;
    int maxSumI = -1;
    
    if (1 == dim)
    {
        return 1;
    }
    
    for (int k = 1; k <= kk; k++)
    {
        cxsc::real sum = 0;

        for (int i = 1; i < k; i++)
        {
            if (i != k)
            {
                sum += cxsc::pow(matrix[i][k], 2);
                sum += cxsc::pow(matrix[k][i], 2);
            }
        }

        sum += cxsc::pow(matrix[k][k], 2);

        if (-1 == maxSumI || maxSum < sum)
        {
            maxSum = sum;
            maxSumI = k;
        }
    }

    return maxSumI;
}

int arg_max_sum_removed_sqr_except(cxsc::rmatrix matrix, std::vector<int> I)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real maxSum = cxsc::MinReal;
    int maxSumI = -1;

    if (1 == dim)
    {
        return 1;
    }

    for (int k = 1; k <= dim; k++)
    {
        if (I.end() != find(I.begin(), I.end(), k))
        {
            continue;
        }

        cxsc::real sum = 0;

        for (int i = 1; i < k; i++)
        {
            if (i != k)
            {
                sum += cxsc::pow(matrix[i][k], 2);
                sum += cxsc::pow(matrix[k][i], 2);
            }
        }

        sum += cxsc::pow(matrix[k][k], 2);

        if (-1 == maxSumI || maxSum < sum)
        {
            maxSum = sum;
            maxSumI = k;
        }
    }

    return maxSumI;
}

int omp_arg_max_sum_removed_sqr(cxsc::rmatrix matrix, int kk)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real maxSum = cxsc::MinReal;
    int maxSumI = -1;
    cxsc::real *sums = new cxsc::real[dim];

    if (1 == dim)
    {
        return 1;
    }

	#pragma omp parallel for
    for (int k = 1; k <= kk; k++)
    {
        sums[k - 1] = 0;
        for (int i = 1; i < k; i++)
        {
            if (i != k)
            {
                sums[k - 1] += matrix[i][k] * matrix[i][k];
                sums[k - 1] += matrix[k][i] * matrix[k][i];
            }
        }
        sums[k - 1] += matrix[k][k] * matrix[k][k];
    }

    for (int i = 0; i < kk; i++)
    {
        if (sums[i] > maxSum || -1 == maxSumI)
        {
            maxSum = sums[i];
            maxSumI = i;
        }
    }

    return maxSumI + 1;
}

int omp_arg_max_sum_removed_sqr_except(cxsc::rmatrix matrix, std::vector<int> I)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real maxSum = cxsc::MinReal;
    int maxSumI = -1;
    cxsc::real *sums = new cxsc::real[dim];

    if (1 == dim)
    {
        return 1;
    }

	#pragma omp parallel for
    for (int k = 1; k <= dim; k++)
    {
        sums[k - 1] = 0;

        if (I.end() != find(I.begin(), I.end(), k))
        {
            sums[k - 1] = cxsc::MinReal;
            continue;
        }

        for (int i = 1; i < k; i++)
        {
            if (i != k)
            {
                sums[k - 1] += matrix[i][k] * matrix[i][k];
                sums[k - 1] += matrix[k][i] * matrix[k][i];
            }
        }
        sums[k - 1] += matrix[k][k] * matrix[k][k];
    }

    for (int i = 0; i < dim; i++)
    {
        if (sums[i] > maxSum || -1 == maxSumI)
        {
            maxSum = sums[i];
            maxSumI = i;
        }
    }

    return maxSumI + 1;
}

int arg_min_submatrix_eig(cxsc::rmatrix matrix, int kk)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;
    
    if (1 == dim)
    {
        return 1;
    }
    
    for (int k = 1; k <= kk; k++)
    {
        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);
        
        if (minEig > eigens[1])
        {
            minEig = eigens[1];
            minEigI = k;
        }
    }

    return minEigI;
}

int arg_min_submatrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;

    if (1 == dim)
    {
        return 1;
    }
    
    for (int k = 1; k <= dim; k++)
    {
        if (I.end() != find(I.begin(), I.end(), k))
        {
            continue;
        }
        
        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);
        
        if (minEig > eigens[1])
        {
            minEig = eigens[1];
            minEigI = k;
        }
    }

    return minEigI;
}

int omp_arg_min_submatrix_eig(cxsc::rmatrix matrix, int kk)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;
    cxsc::real *eigs = new cxsc::real[dim];

    if (1 == dim)
    {
        return 1;
    }

	#pragma omp parallel for schedule(dynamic,1)
    for (int k = 1; k <= kk; k++)
    {
        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);

        eigs[k - 1] = eigens[1];
    }

    for (int i = 0; i < dim; i++)
    {
        if (minEig > eigs[i])
        {
            minEig = eigs[i];
            minEigI = i;
        }
    }

    return minEigI + 1;
}

int omp_arg_min_submatrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;
    cxsc::real *eigs = new cxsc::real[dim];

    if (1 == dim)
    {
        return 1;
    }

	#pragma omp parallel for schedule(dynamic,1)
    for (int k = 1; k <= dim; k++)
    {
        if (I.end() != find(I.begin(), I.end(), k))
        {
            continue;
        }

        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);

        eigs[k - 1] = eigens[1];
    }

    for (int i = 0; i < dim; i++)
    {
        if (minEig > eigs[i])
        {
            minEig = eigs[i];
            minEigI = i;
        }
    }

    return minEigI + 1;
}

int arg_min_submatrix_matrix_eig(cxsc::rmatrix matrix, int kk)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;

    if (1 == dim)
    {
        return 1;
    }

    for (int k = 1; k <= kk; k++)
    {
        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);

        if (minEig > eigens[1])
        {
            minEig = eigens[1];
            minEigI = k;
        }
    }

    return minEigI;
}

int arg_min_submatrix_matrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;

    if (1 == dim)
    {
        return 1;
    }

    for (int k = 1; k <= dim; k++)
    {
        if (I.end() != find(I.begin(), I.end(), k))
        {
            continue;
        }

        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);

        if (minEig > eigens[1])
        {
            minEig = eigens[1];
            minEigI = k;
        }
    }

    return minEigI;
}

int omp_arg_min_submatrix_matrix_eig(cxsc::rmatrix matrix, int kk)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;
    cxsc::real *eigs = new cxsc::real[dim];

    if (1 == dim)
    {
        return 1;
    }

	#pragma omp parallel for schedule(dynamic,1)
    for (int k = 1; k <= kk; k++)
    {
        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);

        eigs[k - 1] = eigens[1];
    }

    for (int i = 0; i < dim; i++)
    {
        if (minEig > eigs[i])
        {
            minEig = eigs[i];
            minEigI = i;
        }
    }

    return minEigI + 1;
}

int omp_arg_min_submatrix_matrix_eig_except(cxsc::rmatrix matrix, std::vector<int> I)
{
    int dim = cxsc::ColLen(matrix);
    cxsc::real minEig = cxsc::MaxReal;
    int minEigI = -1;
    cxsc::real *eigs = new cxsc::real[dim];

    if (1 == dim)
    {
        return 1;
    }

	#pragma omp parallel for schedule(dynamic,1)
    for (int k = 1; k <= dim; k++)
    {
        if (I.end() != find(I.begin(), I.end(), k))
        {
            continue;
        }

        cxsc::rmatrix submatrix = trim(matrix, k);
        cxsc::rvector eigens = get_jacobi_eig(submatrix);

        eigs[k - 1] = eigens[1];
    }

    for (int i = 0; i < dim; i++)
    {
        if (minEig > eigs[i])
        {
            minEig = eigs[i];
            minEigI = i;
        }
    }

    return minEigI + 1;
}
