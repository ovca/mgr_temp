#include "interlacingDirectMatrix.h"

cxsc::rvector interlacing_direct_matrix(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    cxsc::rvector eigenvalues(n);
    cxsc::rmatrix B = get_magnitude(matrix);
    cxsc::rmatrix tmpB = B;
    cxsc::imatrix A = matrix;
    cxsc::imatrix tmpA = A;
    vector<int> I;

    for (int i = 1; i <= n; i++)
    {
    	cxsc::rmatrix aDelta = get_adelta(tmpA);
    	cxsc::rvector eig = get_jacobi_matrix_eig(tmpB);
        cxsc::rvector eig2 = get_jacobi_matrix_eig(get_ac(tmpA));
        cxsc::real rho = get_spectral_radius(aDelta);

        if (eig[1] < eig2[1] + rho)
        {
            eigenvalues[i] = eig[1];
//            logi << "eig: " << i << " step 1: choosing eigen from Proposition 3.2:" << eigenvalues[i][1] << "(rejected:" << eig2[1][1] + rho << ")" << logendl;
        }
        else
        {
            eigenvalues[i] = eig2[1] + rho;
//            logi << "eig: " << i << " step 1: choosing eigen from Theorem 3.1:" << eigenvalues[i][1] << "(rejected:" << eig[1][1] << ")" << logendl;
        }

    	int removeIndex = indexSelectionFunc(tmpB, n - i + 1);
    	tmpB = trim(tmpB, removeIndex);
    	tmpA = trim(tmpA, removeIndex);
    }

    for (int i = 1; i <= n; i++)
    {
    	tmpB = B;
    	tmpA = A;
    	int promisingIndex = indexSelectionExceptFunc(tmpB, I);
    	I.push_back(promisingIndex);

    	cxsc::rmatrix subB = getMatrixWithIndexes(tmpB, I);
    	cxsc::imatrix subA = getMatrixWithIndexes(tmpA, I);

    	cxsc::rmatrix aDelta = get_adelta(subA);
    	cxsc::rvector eig = get_jacobi_matrix_eig(subB);
        cxsc::rvector eig2 = get_jacobi_matrix_eig(get_ac(subA));
        cxsc::real rho = get_spectral_radius(aDelta);

        cxsc::real eigen;
        if (eig[1] < eig2[1] + rho)
        {
            eigen = eig[1];
//            logi << "eig: " << i << " step 2: choosing eigen from Proposition 3.2:" << eigen << "(rejected:" << eig2[1][1] + rho << ")" << logendl;
        }
        else
        {
            eigen = eig2[1] + rho;
//            logi << "eig: " << i << " step 2: choosing eigen from Theorem 3.1:" << eigen << "(rejected:" << eig[1][1] << ")" logendl;
        }

    	if (eigen < eigenvalues[n - i + 1])
    	{
//            logi << "eig: " << dim - i + 1 << " step 3: choosing eigen from step 2:" << eigen << "(rejected:" << eigenvalues[dim - i + 1][1] << ")" << logendl;
    		eigenvalues[n - i + 1] = eigen;
    	}
    	else
    	{
//            logi << "eig: " << dim - i + 1 << " step 3: choosing eigen from step 1:" << eigenvalues[dim - i + 1][1] << "(rejected:" << eigen << ")"logendl;
    	}
    }

//    logi << endl << endl << endl << logendl;

    return eigenvalues;
}

cxsc::rvector interlacing_direct_matrixL(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    cxsc::rvector eigenvalues(n);
    cxsc::rmatrix B = get_magnitude(matrix);
    cxsc::rmatrix tmpB = B;
    cxsc::imatrix A = matrix;
    cxsc::imatrix tmpA = A;
    vector<int> I;

    for (int i = 1; i <= n; i++)
    {
    	cxsc::rmatrix aDelta = get_adelta(tmpA);
    	cxsc::rvector eig = get_jacobi_matrix_eig(tmpB);
        cxsc::rvector eig2 = get_jacobi_matrix_eig(get_ac(tmpA));
        cxsc::real rho = get_spectral_radius(aDelta);

//        logi << "first pass: " << i << logendl;
//        logi << "eig1: " << -eig[1] << logendl;
//        logi << "eig2: " << -eig2[1] - rho << " (" << -eig2[1] << "-" << -rho << ")" << endl << logendl;

        if (-eig[1] > -eig2[1] - rho)
        {
            eigenvalues[i] = -eig[1];
        }
        else
        {
            eigenvalues[i] = -eig2[1] - rho;
        }

    	int removeIndex = indexSelectionFunc(tmpB, n - i + 1);
    	tmpB = trim(tmpB, removeIndex);
    	tmpA = trim(tmpA, removeIndex);
    }

    for (int i = 1; i <= n; i++)
    {
    	tmpB = B;
    	tmpA = A;
    	int promisingIndex = indexSelectionExceptFunc(tmpB, I);
    	I.push_back(promisingIndex);

    	cxsc::rmatrix subB = getMatrixWithIndexes(tmpB, I);
    	cxsc::imatrix subA = getMatrixWithIndexes(tmpA, I);

    	cxsc::rmatrix aDelta = get_adelta(subA);
    	cxsc::rvector eig = get_jacobi_matrix_eig(subB);
        cxsc::rvector eig2 = get_jacobi_matrix_eig(get_ac(subA));
        cxsc::real rho = get_spectral_radius(aDelta);

//
//        logi << "second pass: " << i << logendl;
//        logi << "eig1: " << -eig[1] << logendl;
//        logi << "eig2: " << -eig2[1] - rho << " (" << -eig2[1] << "-" << rho << ")" << logendl;
//        logi << "eig_prev: " << eigenvalues[n - i + 1] << "(" << n - i + 1 << ")" << endl << logendl;

        cxsc::real eigen;
        if (-eig[1] > -eig2[1] - rho)
        {
            eigen = -eig[1];
        }
        else
        {
            eigen = -eig2[1] - rho;
        }

    	if (eigen > eigenvalues[n - i + 1])
    	{
    		eigenvalues[n - i + 1] = eigen;
    	}
    	else
    	{
//            logi << "eig: " << dim - i + 1 << " step 3: choosing eigen from step 1:" << eigenvalues[dim - i + 1][1] << "(rejected:" << eigen << ")"logendl;
    	}
    }

//    logi << endl << endl << endl << logendl;

    return sort_vector(eigenvalues,n);
}

cxsc::rvector omp_interlacing_direct_matrix(cxsc::imatrix matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

    cxsc::rvector eigenvalues(n);
    cxsc::rmatrix B = get_magnitude(matrix);
    cxsc::rmatrix tmpB = B;
    cxsc::imatrix A = matrix;
    cxsc::imatrix tmpA = A;
    vector<int> I;

    vector<cxsc::imatrix> tmpAQueue;
    vector<cxsc::rmatrix> tmpBQueue;
    for (int i = 1; i <= n; i++)
    {
    	tmpAQueue.push_back(cxsc::imatrix(tmpA));
    	tmpBQueue.push_back(cxsc::rmatrix(tmpB));
    	int removeIndex = indexSelectionFunc(tmpB, n - i + 1);
		tmpB = omp_trim(tmpB, removeIndex);
		tmpA = omp_trim(tmpA, removeIndex);
    }

#pragma omp parallel for schedule(dynamic, 1)
    for (int i = 1; i <= n; i++)
    {
    	cxsc::rmatrix aDelta = get_adelta(tmpAQueue[i - 1]);
    	cxsc::rvector eig = get_jacobi_matrix_eig(tmpBQueue[i - 1]);
        cxsc::rvector eig2 = get_jacobi_matrix_eig(get_ac(tmpAQueue[i - 1]));
        cxsc::real rho = get_spectral_radius(aDelta);

        if (eig[1] < eig2[1] + rho)
        {
            eigenvalues[i] = eig[1];
//            logi << "eig: " << i << " step 1: choosing eigen from Proposition 3.2:" << eigenvalues[i][1] << "(rejected:" << eig2[1][1] + rho << ")" << logendl;
        }
        else
        {
            eigenvalues[i] = eig2[1] + rho;
//            logi << "eig: " << i << " step 1: choosing eigen from Theorem 3.1:" << eigenvalues[i][1] << "(rejected:" << eig[1][1] << ")" << logendl;
        }
    }

    tmpAQueue.clear();
    tmpBQueue.clear();
	tmpB = B;
	tmpA = A;
    for (int i = 1; i <= n; i++)
    {
    	int promisingIndex = indexSelectionExceptFunc(tmpB, I);
    	I.push_back(promisingIndex);

    	cxsc::rmatrix subB = omp_getMatrixWithIndexes(tmpB, I);
    	cxsc::imatrix subA = omp_getMatrixWithIndexes(tmpA, I);

    	tmpAQueue.push_back(cxsc::imatrix(subA));
    	tmpBQueue.push_back(cxsc::rmatrix(subB));
    }

#pragma omp parallel for schedule(dynamic, 1)
    for (int i = 1; i <= n; i++)
    {
    	cxsc::rmatrix aDelta = get_adelta(tmpAQueue[i - 1]);
    	cxsc::rvector eig = get_jacobi_matrix_eig(tmpBQueue[i - 1]);
        cxsc::rvector eig2 = get_jacobi_matrix_eig(get_ac(tmpAQueue[i - 1]));
        cxsc::real rho = get_spectral_radius(aDelta);

        cxsc::real eigen;
        if (eig[1] < eig2[1] + rho)
        {
            eigen = eig[1];
//            logi << "eig: " << i << " step 2: choosing eigen from Proposition 3.2:" << eigen << "(rejected:" << eig2[1][1] + rho << ")" << logendl;
        }
        else
        {
            eigen = eig2[1] + rho;
//            logi << "eig: " << i << " step 2: choosing eigen from Theorem 3.1:" << eigen << "(rejected:" << eig[1][1] << ")" logendl;
        }

    	if (eigen < eigenvalues[n - i + 1])
    	{
//            logi << "eig: " << dim - i + 1 << " step 3: choosing eigen from step 2:" << eigen << "(rejected:" << eigenvalues[dim - i + 1][1] << ")" << logendl;
    		eigenvalues[n - i + 1] = eigen;
    	}
    	else
    	{
//            logi << "eig: " << dim - i + 1 << " step 3: choosing eigen from step 1:" << eigenvalues[dim - i + 1][1] << "(rejected:" << eigen << ")"logendl;
    	}
    }

    return sort_vector(eigenvalues,n);
}

cxsc::ivector get_interlacing_direct_matrix_eig(cxsc::imatrix &matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

	cxsc::rvector eigU = interlacing_direct_matrix(matrix, indexSelectionFunc, indexSelectionExceptFunc);
	cxsc::rvector eigL = interlacing_direct_matrixL(-matrix, indexSelectionFunc, indexSelectionExceptFunc);

	return to_interval(eigU, eigL, n);
}

cxsc::ivector get_omp_interlacing_direct_matrix_eig(cxsc::imatrix &matrix, iselectf indexSelectionFunc, iselectexf indexSelectionExceptFunc)
{
    int cols = cxsc::ColLen(matrix);
    int rows = cxsc::RowLen(matrix);
    int n = cols;

    assert(cols == rows);

	cxsc::rvector eigU = omp_interlacing_direct_matrix(matrix, indexSelectionFunc, indexSelectionExceptFunc);
	cxsc::rvector eigL = omp_interlacing_direct_matrix(-matrix, indexSelectionFunc, indexSelectionExceptFunc);

	return to_interval(eigU, eigL, n);
}
